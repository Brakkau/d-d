# D&D

To Start

Clone repository from bitbucket "d-d"

1. Install dependances with `npm install`

2. Start local dev server with `npm start`

3. For production build use `npm run build`

#Extras
Not yet configured but will be in future to run local tests
<!-- Jest tests: `npm test` -->

# CSS Linter

Sass ordering to keep consistency throughout, please follow below guide for correct ordering:

Order:
    - position
    - top
    - right
    - bottom
    - left
    - display
    - width
    - height
    - margin
    - padding

# Important Information
**The Max Nesting we use: 3**
**We will be using: 2 space**
**NO !importants**

#This package is using Airbnb Javascript Style Guide
Style guide is located here:
https://github.com/airbnb/javascript


# CSS Naming convention used is - BEM

For a quick guide on how to use bem, please use the following link:
Example: https://en.bem.info/methodology/quick-start/

# Templates used is handlebars.js

for a quick guide on how to use handlebars, please use the following link:

Example: https://handlebarsjs.com/

# Docker

Install: https://runnable.com/docker/install-docker-on-macos

Here are some documentation to help you get docker running and some command line promps.

Docker Documentation: https://docker-curriculum.com/?fbclid=IwAR0jKKuJHPkaFaBqi0H47JKxOlVB0WnuvAJ134bd7T_G-KlqYZw_anF6dPU#what-are-containers-

Docker Cheat Sheet: https://www.docker.com/sites/default/files/Docker_CheatSheet_08.09.2016_0.pdf

To run docker use
1. `npm run docker`

This will build a new container and spin it up using the localhost.


