FROM node:6.10.0

RUN mkdir /app
WORKDIR /app
COPY . /app/
RUN npm install
ENTRYPOINT ["npm", "run", "docker"]