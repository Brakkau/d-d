import './assets/scss/index.scss';
// import './assets/img/';
import * as functions from './assets/js/scripts';
import '../node_modules/@fortawesome/fontawesome-free/js/all';
// import navIcon from './assets/img/navIcon.jpg';

// navIcon.src = navIcon;


// On Load Function
Document.prototype.ready = (callback) => {
  if (callback && typeof callback === 'function') {
    // eslint-disable-next-line consistent-return
    document.addEventListener('DOMContentLoaded', () => {
      if (document.readyState === 'interactive' || document.readyState === 'complete') {
        return callback();
      }
    });
  }
};

// USE
document.ready(() => {
  functions.toggleNavFire();
  functions.opacityHomePage();
  functions.hideShow();
});
