import hideShowPages from './templates';

const hideShow = () => {
  const panels = document.querySelectorAll('.content-hide-show');

  const loopPanel = () => {
    [...panels].forEach((panel) => {
      if (panel.style.display === 'block') {
        const hidePanel = panel;
        hidePanel.style.display = 'none';
      }
    });
  };

  const navLinks = document.querySelectorAll('.primary-nav nav div ul li a');
  const navPages = document.querySelectorAll('.navPages div');

  navLinks.forEach((a) => {
    a.addEventListener('click', (e) => {
      e.preventDefault();
      // eslint-disable-next-line no-restricted-globals
      history.replaceState(null, '', a.pathname);
      loopPanel();
      hideShowPages();
      [...navPages].forEach((elem) => {
        if (elem.id === a.getAttribute('aria-controls')) {
          const matchPage = elem;
          matchPage.style.display = 'block';
        }
      });
    });
  });
};

export default hideShow;
