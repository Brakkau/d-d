// Maps the pathname to the related DOM node
const pages = {
  '/': document.getElementById('pageHome'),
  '/characters': document.getElementById('pageCharacters'),
  '/characters/quickReferenaceGuides': document.getElementById('pageQuickReferenceGuides'),
  '/characters/spells': document.getElementById('pageSpells'),
  '/rules': document.getElementById('pageRules'),
  '/rules/dungeonMasterHandbook': document.getElementById('pageDungeonMasterHandbook'),
  '/rules/playerHandbook': document.getElementById('pagePlayerHandbook'),
  '/characterCreation': document.getElementById('pageCharacterCreation'),
};

/**
 * Takes in the page's URL pathname and returns the matching DOM node
 * @param {string} pathName - The window.location.pathname of the page to display
 * @returns {HTMLDivElement} - The DOM node matching the page
 */
const getPageNodeByUrlPath = pathName => pages[pathName];

/**
 * Hides all page elements
 */
const hideAllPages = () => {
  const pageElements = document.getElementsByClassName('js-page-content');

  for (let i = 0; i < pageElements.length; i += 1) {
    pageElements[i].style.display = 'none';
  }
};

/**
 * Accepts a string matching a pages window.location.pathname and returns the relevant DOM node
 * @param {string} pathName - A string matching the required pages pathname
 */
const showPage = (pathName) => {
  getPageNodeByUrlPath(pathName).style.display = 'block';
};

/**
 * Accepts a string matching a pages window.location.pathname then handles the page change
 * @param {string} pathName - A string matching the required pages pathname
 */
const changePage = (pathName) => {
  hideAllPages();
  showPage(pathName);
};

export default changePage;
