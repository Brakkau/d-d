import $ from 'jquery';
import '../External/turn';

const flipBook = () => {
  $('.flipbook').turn({
    width: 800,
    height: 700,
    autoCenter: true,
  });
};

export default flipBook;
