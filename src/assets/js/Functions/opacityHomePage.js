// Front Page Opacity Change
const opacityHomePage = () => {
  const boxes = document.querySelectorAll('.box');
  // // 100 Height Box
  // const homePageBox1 = document.getElementsByClassName('box column1')[0];
  // // 33.33 Height boxes
  // const homePageBox2 = document.getElementsByClassName('box column2 row1')[0];
  // const homePageBox3 = document.getElementsByClassName('box column2 row2')[0];
  // const homePageBox4 = document.getElementsByClassName('box column2 row3')[0];

  boxes.forEach((value) => {
    value.addEventListener('mouseover', (e) => {
      e.currentTarget.classList.toggle('hoverOpacityHalf');
      e.currentTarget.classList.remove('hoverOpacityFull');
    });
  });

  boxes.forEach((value) => {
    value.addEventListener('mouseout', (e) => {
      e.currentTarget.classList.toggle('hoverOpacityFull');
      e.currentTarget.classList.remove('hoverOpacityHalf');
    });
  });
};

export default opacityHomePage;
