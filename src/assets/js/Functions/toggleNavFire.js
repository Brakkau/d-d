// Toggle Nav Open and Closed
const toggleNavFire = () => {
  const navToggleClass = document.getElementsByClassName('nav-toggle')[0];

  navToggleClass.addEventListener('click', (e) => {
    e.preventDefault();
    const HTMLClass = document.getElementsByClassName('html')[0];
    HTMLClass.classList.toggle('openNav');
    navToggleClass.classList.toggle('active');
  });
};

export default toggleNavFire;
