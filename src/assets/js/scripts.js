import changePage from './Functions/pageHelpers';
import toggleNavFire from './Functions/toggleNavFire';
import opacityHomePage from './Functions/opacityHomePage';
import flipBook from './Functions/flipBook';

const hideShow = () => {
  // Uses Array.from() as browser support for forEach on NodeList is still a bit flakey
  const navLinks = Array.from(document.querySelectorAll('.primary-nav nav div ul li a'));

  navLinks.forEach((navLink) => {
    navLink.addEventListener('click', (e) => {
      const { pathname } = navLink;

      e.preventDefault();
      // eslint-disable-next-line no-restricted-globals
      history.replaceState(null, '', pathname);
      changePage(pathname);
    });
  });
};

module.exports = {
  toggleNavFire,
  opacityHomePage,
  hideShow,
  flipBook,
};
